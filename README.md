# nodejs-sample-app-cicd



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Collaborate with me on this project to make it better

- [click here to collaborate on this project via hackmd](https://hackmd.io/@6eZuVX-MQu2bSJPsbXpq1w/BklLotZhj)

## NodeJS Sample App with GitLab CICD

## Description
This is a simple nodeJS web app running in a container, build for deploying to any infrastructure


## Visuals
- [link to lucidchart](https://lucidchart.com)

## Deploy Steps
To deploy this app, follow these steps:
1. 
2. 
3. 

## End State
Here's an example of the end state of this application after its been deployed and configured.

## License
MIT
